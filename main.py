import sys
import math
import pygame as pg

pg.init()
ww, wh = pg.display.Info().current_w - 50, pg.display.Info().current_h - 100
mainScreen = pg.display.set_mode((ww, wh))


class Point:
    pass


class FourierConst:
    def __str__(self):
        return f"N: {self.frequency} \nR: {self.r}\rI: {self.i}\n"

    def __repr__(self):
        return str(self)

    def __init__(self, frequency, r, i):
        self.frequency = frequency
        self.r = r
        self.i = i

    def getVector(self, t):
        angle = math.pi * 2 * self.frequency * t
        cosX = math.cos(angle)
        sinX = math.sin(angle)
        return Point(self.r * cosX - self.i * sinX, self.r * sinX + self.i * cosX)


class Point:
    x: float
    y: float

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def clone(self):
        return Point(self.x, self.y)

    def tuple(self):
        return self.x, self.y

    def applyVector(self, p: Point):
        self.x += p.x
        self.y += p.y
        return self




def getVectors(startPoint: Point, fs , ctime):
    points = [startPoint]
    for fc in fs:
        points.append(points[-1].clone().applyVector(fc.getVector(ctime)))
    return points


center = Point(ww / 2, wh / 2)

mainDraw = []


def calculFourier(points):
    pts = points + points[::-1]
    rst = []
    n = 0
    # pour une frequence N on calcule le centre de masse

    #             / 1             (0, -n x 2pi x tt)
    #  moy_n =   /     pts(tt) x e                    x steptt
    #          _/0
    while n <= max(3, len(pts) / 10):
        moy = 0 + 0j
        tt = 0
        steptt = 1 / len(pts)
        for p in pts:
            val = p.x - center.x + (p.y - center.y) * 1j
            moy += val * (math.e ** ((-n * 2.0 * math.pi * tt) * 1j)) * (steptt)
            tt += steptt
        rst.append(FourierConst(n, moy.real, moy.imag))
        if n > 0:
            n = n * -1
        else:
            n = n * -1
            n += 1

    return rst


modeArtist = 0
drawPoints = []
selectedSeries = 0
changeDetails = 0
ffont = pg.font.Font(pg.font.get_default_font(),24)
fakeTime = 0
while 1:
    doNotConnectDot = False
    fakeTime += 0.001
    if fakeTime > 0.5 :
        fakeTime=0
        doNotConnectDot = True
    for e in pg.event.get():
        if e.type == pg.QUIT:
            sys.exit(0)

        if e.type == pg.KEYDOWN:
            if e.key == pg.K_LEFT:
                changeDetails = -1

            if e.key == pg.K_RIGHT:
                changeDetails = 1
        if e.type == pg.KEYUP:
            changeDetails = 0


            if e.key == 9:
                selectedSeries = (selectedSeries + 1) % len(mainDraw)
            if e.key == 8:
                if selectedSeries >= 0 and selectedSeries < len(mainDraw):
                    mainDraw.pop(selectedSeries)
                    if selectedSeries > 0:
                        selectedSeries -= 1

        if modeArtist:
            if getattr(e, "pos", False):
                drawPoints.append(Point(e.pos[0], e.pos[1]))
        if e.type == pg.MOUSEBUTTONDOWN:
            modeArtist = 1
            drawPoints = []
        if e.type == pg.MOUSEBUTTONUP:
            modeArtist = 0
            vecs = calculFourier(drawPoints)
            mainDraw.append({"mainFs": vecs, "shapes": [], "details": len(vecs)})
            selectedSeries = len(mainDraw)-1
            fakeTime = 0

    mainScreen.fill([0, 0, 0])

    if changeDetails != 0 :
        for dd in range (len(mainDraw)):
            mainDraw[dd]["details"] = min(
                max(mainDraw[dd]["details"] +changeDetails, 2),
                len(mainDraw[dd]["mainFs"]))
            mainDraw[dd]["shapes"] = []
            mainDraw[dd]["shapesOLD"] = []
            fakeTime=0
    if modeArtist:
        for i in range(len(drawPoints) - 1):
            pg.draw.line(mainScreen, [0, 0, 255], drawPoints[i].tuple(), drawPoints[i + 1].tuple(), 2)

    for ind, dd in enumerate(mainDraw):
        col = [100, 100, 100]
        if ind == selectedSeries:
            col = [255, 0, 0]
        vectors = getVectors(center, dd["mainFs"],fakeTime)
        for i in range(dd["details"] - 2):
            pg.draw.line(mainScreen, col, vectors[i + 1].tuple(), vectors[i + 2].tuple())

        if doNotConnectDot:
            dd["shapesOLD"] = dd["shapes"]
            dd["shapes"] = []
        dd["shapes"].append(vectors[dd["details"] - 1].clone())
        shapesOld = dd.get("shapesOLD", [])
        for i in range(len(shapesOld) - 1):
            pg.draw.line(mainScreen, [255, 255, 0], shapesOld[i].tuple(), shapesOld[i + 1].tuple(), 2)
        for i in range(len(dd["shapes"]) - 1):
            pg.draw.line(mainScreen, [255, 255, 0], dd["shapes"][i].tuple(), dd["shapes"][i + 1].tuple(), 2)


    pg.display.flip()
